# Week starting September 27 ️🚀

#### Main focus 

- Transition Growth groups to other writers
- Secure issues for 13.5
- Growth issues for 13.5
- Improve structure of Secure docs

#### Milestone 

13.5

## Tasks

#### Design

- [x] 

#### Other

- [x] Move forward an OKR MR

#### Learning and Development

- [x] Watched the [DAST Landscape Brownbag Session](https://www.youtube.com/watch?v=hijw29-8Ses) video to learn about DAST, especially current challenges.

## Wins and Lessons

## Wins of the week

- 

#### What I learnt

- 

#### What did not go so well? Why?

- 

-----------

## Next Week

- [ ] 

## Plan for later

- [ ]

## Current OKRs to focus on

#### UX

- [UX Q3 OKRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8363)
