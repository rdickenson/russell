# Week starting November 1 ️🚀

#### Main focus 

- Secure issues for 13.6
- Fulfillment issues for 13.6

#### Milestone 

13.6

## Tasks

#### Writing

- [x] 

#### Other

#### Learning and Development

- [ ] Complete annual performance evaluation sheet
- [ ] Watch a Secure brown bag session
- [ ] Watch a "Kubernetes 101" video

#### Meetings

- [ ] 1:1 with Craig

## Wins and Lessons

## Wins of the week

- 

#### What I learnt

- 

#### What did not go so well? Why?

- 

-----------

## Next Week

- [ ] 

## Plan for later

- [ ]

## Current OKRs to focus on

#### UX

- [UX Q3 OKRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8363)